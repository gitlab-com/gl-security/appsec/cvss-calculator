# The CVSS calculator project has been relocated

Please update your bookmarks and links.

- WebApp: https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/
- Repository: https://gitlab.com/gitlab-com/gl-security/product-security/appsec/cvss-calculator/
